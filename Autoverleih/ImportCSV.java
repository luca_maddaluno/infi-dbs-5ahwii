	import java.io.*;

	public class ImportCSV {
	  String pfad;
	  int spalten;

	 	  public ImportCSV(String pfad, int spalten)
	  {
	    this.pfad = pfad;
	    this.spalten = spalten;
	  }

	  

	  public void ausgeben () throws IOException {
	       BufferedReader br = null;
	       String line = "";
	       String csvSplitBy = ";";
	       
	       br = new BufferedReader(new FileReader(pfad));
	       
	       while((line = br.readLine()) != null)
	       {
	    	   
	    	   String[] value = line.split(csvSplitBy);
	    	   for (int i = 0; i<spalten; i++)
	    	   {
	    		   System.out.print(value[i] + " "); 
	    	   }
	    	   System.out.println();
	       }
	       
	  }
}