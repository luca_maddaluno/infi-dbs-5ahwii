
public class Fahrzeugdaten 
{
	//Variablen
	String marke;
	String modell;
	int sitzpl�tze;
	
	//Konstruktor
	public Fahrzeugdaten( String marke, String modell, int sitzpl�tze) {
		super();
		this.marke = marke;
		this.modell = modell;
		this.sitzpl�tze = sitzpl�tze;
	}
	
	//Getter und Setter
	
	//Marke
	public String getMarke() {
		return marke;
	}
	public void setMarke(String marke) {
		this.marke = marke;
	}
	//Modell
	public String getModell() {
		return modell;
	}
	public void setModell(String modell) {
		this.modell = modell;
	}
	//Sitzpl�tze
	public int getSitzpl�tze() {
		return sitzpl�tze;
	}
	public void setSitzpl�tze(int sitzpl�tze) {
		this.sitzpl�tze = sitzpl�tze;
	}
	
	
	
}
