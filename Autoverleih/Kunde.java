

public class Kunde 
{
	//Variablen
	String email;
	String vorname;
	String nachname;
	String passwort;
	int plz;
	String stra�e;
	int hausnummer;

	
	
	//Konstruktor
	public Kunde(String email, String vorname, String nachname, String passwort, int plz, String stra�e, int hausnummer) {
		super();
		this.email = email;
		this.vorname = vorname;
		this.nachname = nachname;
		this.passwort = passwort;
		this.plz = plz;
		this.stra�e = stra�e;
		this.hausnummer = hausnummer;
	
	}
	
	
	//Getter und Setter
	
	//Email
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	//Vorname
	public String getVorname() {
		return vorname;
	}
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	//Nachname
	public String getNachname() {
		return nachname;
	}
	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	//Passwort
	public String getPasswort() {
		return passwort;
	}
	public void setPasswort(String passwort) {
		this.passwort = passwort;
	}
	
	//Postleitzahl
	public int getPlz() {
		return plz;
	}
	public void setPlz(int plz) {
		this.plz = plz;
	}
	//Stra�e
	public String getStra�e() {
		return stra�e;
	}
	public void setStra�e(String stra�e) {
		this.stra�e = stra�e;
	}
	//Hausnummer
	public int getHausnummer() {
		return hausnummer;
	}
	public void setHausnummer(int hausnummer) {
		this.hausnummer = hausnummer;
	}
	
	


	

}
