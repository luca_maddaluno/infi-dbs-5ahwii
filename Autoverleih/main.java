import java.io.IOException;
import java.sql.SQLException;

public class main {

	public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException {
		
		ImportCSV csvFahrzeug = new ImportCSV("Fahrzeugdaten.csv", 4);
		ImportCSV csvKunde = new ImportCSV("Kunden.csv", 7);
		ImportCSV csvGeliehen = new ImportCSV("geliehen.csv", 5);
		ImportCSV csvReparatur = new ImportCSV("Reparatur.csv", 4);
		
		AutoverleihManager m1 = new AutoverleihManager();
		
		m1.kundeInDB("Kunden.csv", 7);
		m1.fahrzeugInDB("Fahrzeugdaten.csv", 4);
		m1.geliehenInDB("geliehen.csv", 5);
		m1.reparaturInDB("Reparatur.csv", 4);
		
		String[] kunden = m1.getKunden();
		
		for(int i = 0; i<kunden.length; i++)
		{
			System.out.println(kunden[i]);
		}
		
		csvFahrzeug.ausgeben();
		System.out.println();
		csvGeliehen.ausgeben();
		System.out.println();
		csvKunde.ausgeben();
		System.out.println();
		csvReparatur.ausgeben();

	}

}
