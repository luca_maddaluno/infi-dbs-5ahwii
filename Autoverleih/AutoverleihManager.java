import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class AutoverleihManager 
{
	

		Connection c;
		
		public AutoverleihManager() throws ClassNotFoundException, SQLException 
		{
		
		    try {
		      Class.forName("org.sqlite.JDBC");
		      c = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\lunol\\Desktop\\Schule\\HTL_Anichstra�e\\SWPS\\3Klasse\\M�ller\\�bungen\\Autoverleih INFI\\autoverleih.db");
		      c.setAutoCommit(false);
		      System.out.println("Opened database successfully");
		      
		    } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);
		    }
		    create();
		}
		
		public void create() throws SQLException
		{
			c= DriverManager.getConnection("jdbc:sqlite:autoverleih.db");
			try{
				
			String sql = "Create Table IF NOT EXISTS Kunden(Email varchar(150) Primary Key NOT NULL,"
					+ "Vorname varchar(100) NOT NULL,Nachname varchar(100) NOT NULL,"
					+ "Passwort varchar(20) NOT NULL,Postleitzahl INT NOT NULL,"
					+ "Stra�e varchar(100) NOT NULL,Hausnummer INT NOT NULL);";
			Statement stmt = c.createStatement();
			stmt.executeUpdate(sql);
			
			sql = "Create Table IF NOT EXISTS Fahrzeugdaten("
					+"FahrzeugID INTEGER Primary Key AUTOINCREMENT,"
					+"Marke varchar(100) NOT NULL,"
					+"Modell varchar(100) NOT NULL,"
					+"Sitzpl�tze INT NOT NULL);";
			stmt = c.createStatement();
			stmt.executeUpdate(sql);
			
			sql = "Create Table IF NOT EXISTS Reparatur("
					+"ReparaturID INTEGER Primary Key AUTOINCREMENT,"
					+"Bezeichnung varchar(100) NOT NULL,"
					+"Kosten DECIMAL NOT NULL,"
					+"FahrzeugID INT NOT NULL,"
					+"Foreign Key(FahrzeugID) References Fahrzeugdaten(FahrzeugID) ON DELETE Cascade);";
			
			stmt = c.createStatement();
			stmt.executeUpdate(sql);
			
			sql = "Create Table IF NOT EXISTS Geliehen("
					+"Email varchar(150) NOT NULL,"
					+"FahrzeugID INT NOT NULL,"
					+"Datum Date NOT NULL,"
					+"Primary Key (Email, FahrzeugID, Datum),"
					+"Foreign Key(Email) References Kunden(Email) On DELETE Cascade,"
					+"Foreign Key (FahrzeugID) References Fahrzeugdaten(FahrzeugID) On DELETE Cascade);";
			
			stmt = c.createStatement();
			stmt.executeUpdate(sql);
			
			stmt.close();
			
			System.out.println("created successfully");
			} 
			catch ( Exception e ) {
			      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			      System.exit(0);
			    }
			c.close();
		}
		
		public void hinzuf�genKunde(Kunde kunde) throws SQLException
		{
			c= DriverManager.getConnection("jdbc:sqlite:autoverleih.db");
			String sql = "INSERT or REPLACE INTO Kunden Values(?,?,?,?,?,?,?)";
			PreparedStatement stmt = c.prepareStatement(sql);
			stmt.setString(1, kunde.getEmail());
			stmt.setString(2, kunde.getVorname());
			stmt.setString(3, kunde.getNachname());
			stmt.setString(4, kunde.getPasswort());
			stmt.setInt(5, kunde.getPlz());
			stmt.setString(6, kunde.getStra�e());
			stmt.setInt(7, kunde.getHausnummer());
			stmt.executeUpdate();
			stmt.close();
			c.close();
		}
		
		public void hinzuf�genAuto(Fahrzeugdaten f) throws SQLException
		{
			c= DriverManager.getConnection("jdbc:sqlite:autoverleih.db");
			String sql = "INSERT or REPLACE INTO Fahrzeugdaten Values (?,?,?,?)" ;
			PreparedStatement stmt = c.prepareStatement(sql);
			stmt.setString(2, f.getMarke());
			stmt.setString(3, f.getModell());
			stmt.setInt(4, f.getSitzpl�tze());
			stmt.executeUpdate();
			stmt.close();
			c.close();
		}
		
		public void leihen(int fahrzeugID, String email, Date date) throws SQLException
		{
			c= DriverManager.getConnection("jdbc:sqlite:autoverleih.db");
			String sql  ="INSERT or REPLACE INTO Geliehen VALUES(?,?,?)";
			PreparedStatement stmt = c.prepareStatement(sql);
			stmt.setString(1, email);
			stmt.setInt(2, fahrzeugID);
			stmt.setDate(3, date);
			stmt.executeUpdate();
			stmt.close();
			c.close();
		}
		
		public void reparieren(String bezeichnung, int kosten, int fahrzeugID) throws SQLException
		{
			c= DriverManager.getConnection("jdbc:sqlite:autoverleih.db");
			String sql  ="INSERT or REPLACE INTO Reparatur VALUES(?,?,?,?)";
			PreparedStatement stmt = c.prepareStatement(sql);
			stmt.setString(2, bezeichnung);
			stmt.setInt(3, kosten);
			stmt.setInt(4, fahrzeugID);
			stmt.executeUpdate();
			stmt.close();
			c.close();
		}
		
		public void kundeInDB(String pfad, int spalten) throws IOException, SQLException
		{
			   BufferedReader br = null;
		       String line = "";
		       String csvSplitBy = ";";
		       
		       br = new BufferedReader(new FileReader(pfad));
		       
		       while((line = br.readLine()) != null)
		       {
		    	   
		    	   String[] value = line.split(csvSplitBy);
		    	   Kunde k = new Kunde(value[0], value[1], value[2], value[3], 
		    			   Integer.parseInt(value[4]), value[5],Integer.parseInt(value[6]));
		    	   hinzuf�genKunde(k);
		    	   System.out.println();
		       }
		       
		       System.out.println("imported Kunden successfully");
		}
		
		public void fahrzeugInDB(String pfad, int spalten) throws NumberFormatException, IOException, SQLException
		{
			BufferedReader br = null;
		       String line = "";
		       String csvSplitBy = ";";
		       
		       br = new BufferedReader(new FileReader(pfad));
		       
		       while((line = br.readLine()) != null)
		       {
		    	   
		    	   String[] value = line.split(csvSplitBy);
		    	   Fahrzeugdaten f = new Fahrzeugdaten(value[1], value[2], Integer.parseInt(value[3]));
		    	   hinzuf�genAuto(f);
		    	   System.out.println();
		       }
		       
		       System.out.println("importedFahrzeugdaten successfully");
		}
		
		public void reparaturInDB(String pfad, int spalten) throws NumberFormatException, IOException, SQLException
		{
			BufferedReader br = null;
		       String line = "";
		       String csvSplitBy = ";";
		       
		       br = new BufferedReader(new FileReader(pfad));
		       
		       while((line = br.readLine()) != null)
		       {
		    	   
		    	   String[] value = line.split(csvSplitBy);
		    	   reparieren(value[1], Integer.parseInt(value[2]), Integer.parseInt(value[3]));
		    	   System.out.println();
		       }
		       
		       System.out.println("imported Reparatur successfully");
		}
		
		public void geliehenInDB(String pfad, int spalten) throws NumberFormatException, IOException, SQLException
		{
			BufferedReader br = null;
		       String line = "";
		       String csvSplitBy = ";";
		       
		       br = new BufferedReader(new FileReader(pfad));
		       
		       while((line = br.readLine()) != null)
		       {
		    	   
		    	   String[] value = line.split(csvSplitBy);
		    	   leihen(Integer.parseInt(value[1]), value[0], new Date((Integer.parseInt(value[4])-1900), (Integer.parseInt(value[3])-1), Integer.parseInt(value[2])));
		    	   System.out.println();
		       }
		       
		       System.out.println("imported geliehen successfully");
		}
		
		public String[] getKunden() throws SQLException
		{
			c= DriverManager.getConnection("jdbc:sqlite:autoverleih.db");
			int i = 0;
			String[] kunden= new String[10];
			String sql = "Select * FROM Kunden";
			PreparedStatement stmt = c.prepareStatement(sql); 
			ResultSet rs = stmt.executeQuery();
			while(rs.next())
			{
				kunden[i] = rs.getString(1) + ", " + rs.getString(2)+ ", " + rs.getString(3)
				+ ", " + rs.getString(4) +  ", " + rs.getInt(5) + ", " + rs.getString(6)+  ", " + rs.getInt(7) ;
				
			}
			stmt.close();
			c.close();
			return kunden;
		}
}

